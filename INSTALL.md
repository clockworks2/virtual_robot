# Installation Instructions

## Prerequisites

1. JDK 11 or newer ([Linux tarball, deb or rpm](https://www.azul.com/downloads/zulu-community/?version=java-15-mts&os=windows&architecture=x86-64-bit&package=jdk)), [Windows](https://www.azul.com/downloads/zulu-community/?version=java-15-mts&os=windows&architecture=x86-64-bit&package=jdk))
2. Intellij IDEA Community, Ultimate or another IDE that supports Maven projects

#### Note

If possible, prefer installing the JDK using your distro's package manager.

## Installation

Clone this repository and open in your IDE. Maven will install all required dependencies.

## Running

Select the main class as `virtual_robot.controller.Main` and run. You may need to specify the classpath module as `teamcode`.

Alternatively, if running from the command line or packaging into a jar, run `mvn package`
and then run the final jar (`java -jar teamcode/target/teamcode-jar-with-dependencies.jar`)
