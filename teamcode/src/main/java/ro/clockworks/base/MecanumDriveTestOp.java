/**
 *
 * Copyright (C) Clockworks 2020-present
 * This file is part of ro.clockworks.base package
 *
 * ro.clockworks.base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ro.clockworks.base is distributed in the hope that it will be useful,
 * but without any warranty, without even the implied warranty of
 * merchantability or fitness for a particular purpose. See the
 * GNU General Public License for more details.
 *     _____  _               _                            _
 *    /  __ \| |             | |                          | |
 *    | /  \/| |  ___    ___ | | ____      __  ___   _ __ | | __ ___
 *    | |    | | / _ \  / __|| |/ /\ \ /\ / / / _ \ | '__|| |/ // __|
 *    | \__/\| || (_) || (__ |   <  \ V  V / | (_) || |   |   < \__ \
 *     \____/|_| \___/  \___||_|\ \  \_/\_/   \___/ |_|   |_|\_\|___/
 *                               \ \
 *                                \_\ yeah it works
 */

package ro.clockworks.base;


import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@TeleOp(name = "MecanumDriveTestOp", group = "Clockworks")
public class MecanumDriveTestOp extends LinearOpMode {
    MecanumDrivetrain drive = new MecanumDrivetrain();

    @Override
    public void runOpMode() throws InterruptedException {
        drive.init(hardwareMap);

        while (!gamepad1.x) {
            double angle = AngleUtils.toDegs(AngleUtils.angleOf(-gamepad1.left_stick_y, -gamepad1.left_stick_x));
            double power = Math.sqrt(gamepad1.left_stick_x * gamepad1.left_stick_x + gamepad1.left_stick_y * gamepad1.left_stick_y);

            drive.drive(power, angle, -gamepad1.right_stick_x);
        }

    }

}
