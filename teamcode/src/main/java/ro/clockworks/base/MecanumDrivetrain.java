/**
 *
 * Copyright (C) Clockworks 2020-present
 * This file is part of ro.clockworks.base package
 *
 * ro.clockworks.base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ro.clockworks.base is distributed in the hope that it will be useful,
 * but without any warranty, without even the implied warranty of
 * merchantability or fitness for a particular purpose. See the
 * GNU General Public License for more details.
 *     _____  _               _                            _
 *    /  __ \| |             | |                          | |
 *    | /  \/| |  ___    ___ | | ____      __  ___   _ __ | | __ ___
 *    | |    | | / _ \  / __|| |/ /\ \ /\ / / / _ \ | '__|| |/ // __|
 *    | \__/\| || (_) || (__ |   <  \ V  V / | (_) || |   |   < \__ \
 *     \____/|_| \___/  \___||_|\ \  \_/\_/   \___/ |_|   |_|\_\|___/
 *                               \ \
 *                                \_\ yeah it works
 */

package ro.clockworks.base;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;

import java.util.Arrays;

public class MecanumDrivetrain {
    double speeds[] = new double[4];
    private DcMotor frontLeft;
    private DcMotor frontRight;
    private DcMotor backRight;
    private DcMotor backLeft;
    private BNO055IMU imu;
    private double zeroHeading;

    void init(HardwareMap hwMap) {
        imu = hwMap.get(BNO055IMU.class, "imu");
        BNO055IMU.Parameters params = new BNO055IMU.Parameters();
        params.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        params.angleUnit = BNO055IMU.AngleUnit.RADIANS;
        imu.initialize(params);
        zeroHeading = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZXY, AngleUnit.RADIANS).firstAngle;

        frontLeft = hwMap.get(DcMotor.class, "front_left_motor");
        frontRight = hwMap.get(DcMotor.class, "front_right_motor");
        backLeft = hwMap.get(DcMotor.class, "back_left_motor");
        backRight = hwMap.get(DcMotor.class, "back_right_motor");

        frontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        frontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        backLeft.setDirection(DcMotor.Direction.REVERSE);
        frontLeft.setDirection(DcMotor.Direction.REVERSE);

        speeds[0] = speeds[1] = speeds[2] = speeds[3] = 0;
        applySpeeds();
    }

    public void drive(double power, double angleDegs, double rotate) {
        power = Math.min(1, Math.max(0, power));
        rotate = Math.min(1, Math.max(-1, rotate));
        double rads = AngleUtils.toRads(angleDegs + 45);
        speeds[0] = Math.cos(rads) * power - rotate;
        speeds[1] = Math.sin(rads) * power + rotate;
        speeds[2] = Math.sin(rads) * power - rotate;
        speeds[3] = Math.cos(rads) * power + rotate;
        normSpeeds();
        applySpeeds();
    }

    private void normSpeeds() {
        double max = Arrays.stream(speeds).map(Math::abs).max().orElse(0);
        if (max > 1) {
            for (int i = 0; i < speeds.length; i++) {
                speeds[i] /= max;
            }
        }
    }

    private void applySpeeds() {
        frontLeft.setPower(speeds[0]);
        frontRight.setPower(speeds[1]);
        backLeft.setPower(speeds[2]);
        backRight.setPower(speeds[3]);
    }

    public double getHeading() {
        return AngleUtils.toDegs(AngleUtils.normalizeRads(imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZXY, AngleUnit.RADIANS).firstAngle - zeroHeading));
    }
}
