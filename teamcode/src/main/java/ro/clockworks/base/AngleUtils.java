/**
 *
 * Copyright (C) Clockworks 2020-present
 * This file is part of ro.clockworks.base package
 *
 * ro.clockworks.base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ro.clockworks.base is distributed in the hope that it will be useful,
 * but without any warranty, without even the implied warranty of
 * merchantability or fitness for a particular purpose. See the
 * GNU General Public License for more details.
 *     _____  _               _                            _
 *    /  __ \| |             | |                          | |
 *    | /  \/| |  ___    ___ | | ____      __  ___   _ __ | | __ ___
 *    | |    | | / _ \  / __|| |/ /\ \ /\ / / / _ \ | '__|| |/ // __|
 *    | \__/\| || (_) || (__ |   <  \ V  V / | (_) || |   |   < \__ \
 *     \____/|_| \___/  \___||_|\ \  \_/\_/   \___/ |_|   |_|\_\|___/
 *                               \ \
 *                                \_\ yeah it works
 */

package ro.clockworks.base;

public interface AngleUtils {
    double PI = Math.PI;
    double TWO_PI = Math.PI * 2;

    /**
     * Convert degrees to radians
     * @param degs angle in degrees
     * @return angle in radians
     */
    static double toRads(double degs) {
        return degs * PI / 180;
    }

    /**
     * Convert radians to degrees
     * @param rads andle in radians
     * @return angle in degrees
     */
    static double toDegs(double rads) {
        return rads * 180 / PI;
    }

    /**
     * Normalize an angle in radian space [-PI, PI]
     * @param rads angle in radians
     * @return normalized angle in radians
     */
    static double normalizeRads(double rads) {
        double norm = (rads % TWO_PI + TWO_PI) % TWO_PI;
        return norm <= PI ? norm : norm - TWO_PI;
    }

    /**
     * Normalize an angle in degree space [-180, 180]
     * @param degs angle in degrees
     * @return normalized angle in degrees
     */
    static double normalizeDegs(double degs) {
        return toDegs(normalizeRads(toRads(degs)));
    }

    /**
     * Calculate the angle from the X axis to the point given
     * @param x X coordinate of the point
     * @param y Y coordinate of the point
     * @return the calculated angle, in radians
     */
    static double angleOf(double x, double y) {
        return Math.atan2(y, x);
    }

    /**
     * Calculates the shortest signed distance angle of two angles
     * @param from source of rotation, in radians
     * @param to destination of rotation, in radians
     * @return length of rotation, in radians
     */
    static double distanceRads(double from, double to) {
        double f = normalizeRads(from);
        double t = normalizeRads(to);
        double dist = t - f;
        while (dist > PI) dist -= TWO_PI;
        while (dist < -PI) dist += TWO_PI;
        return dist;
    }

    /**
     * Calculates the shortest signed distance angle of two angles
     * @param from source of rotation, in degrees
     * @param to destination of rotation, in degrees
     * @return length of rotation, in degrees
     */
    static double distanceDegs(double from, double to) {
        return toDegs(distanceRads(toRads(from), toRads(to)));
    }
}
