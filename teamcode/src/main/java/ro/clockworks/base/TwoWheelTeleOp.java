/**
 *
 * Copyright (C) Clockworks 2020-present
 * This file is part of ro.clockworks.base package
 *
 * ro.clockworks.base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ro.clockworks.base is distributed in the hope that it will be useful,
 * but without any warranty, without even the implied warranty of
 * merchantability or fitness for a particular purpose. See the
 * GNU General Public License for more details.
 *     _____  _               _                            _
 *    /  __ \| |             | |                          | |
 *    | /  \/| |  ___    ___ | | ____      __  ___   _ __ | | __ ___
 *    | |    | | / _ \  / __|| |/ /\ \ /\ / / / _ \ | '__|| |/ // __|
 *    | \__/\| || (_) || (__ |   <  \ V  V / | (_) || |   |   < \__ \
 *     \____/|_| \___/  \___||_|\ \  \_/\_/   \___/ |_|   |_|\_\|___/
 *                               \ \
 *                                \_\ yeah it works
 */

package ro.clockworks.base;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

@TeleOp(name = "TwoWheelTestOp", group = "Clockworks")
public class TwoWheelTeleOp extends LinearOpMode {

    @Override
    public void runOpMode() throws InterruptedException {
        DcMotor left = hardwareMap.get(DcMotor.class, "left_motor");
        DcMotor right = hardwareMap.get(DcMotor.class, "right_motor");
        left.setDirection(DcMotorSimple.Direction.REVERSE);

        left.setPower(0.5);
        right.setPower(0.5);

        Thread.sleep(1000);

        left.setPower(0);
        left.setPower(0);

    }
}
